<?php

use App\Models\DocumentType;
use App\Repositories\DocumentTypeRepository;

/**
 * Setup the database state before each test
 */
beforeEach(fn () => Artisan::call('migrate', ['--env' => 'testing']));

/**
 * CREATE
 */
it('creates and retrieves a document type', function () {
    $repo = new DocumentTypeRepository();
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice'
    ];

    $documentType = $repo->create($data);

    expect($documentType)->toBeInstanceOf(DocumentType::class)
        ->and($documentType->name)
        ->toBe($data['name'])
        ->and($documentType->pdf_template)
        ->toBe($data['pdf_template']);

    $retrievedDocumentType = $repo->findById($documentType->id);

    expect($retrievedDocumentType->name)
        ->toBe($data['name'])
        ->and($retrievedDocumentType->pdf_template)
        ->toBe($data['pdf_template']);
});
/**
 * GET ALL
 */
it('retrieves all document types', function () {
    $repo = new DocumentTypeRepository();

    $data1 = ['name' => 'Invoice1', 'pdf_template' => 'Template content for Invoice1'];
    $data2 = ['name' => 'Invoice2', 'pdf_template' => 'Template content for Invoice2'];

    $repo->create($data1);
    $repo->create($data2);

    $allDocumentTypes = $repo->getAll();

    expect($allDocumentTypes)->toHaveCount(2);
});
/**
 * UPDATE using Model instance
 */
it('updates a document type using Model instance', function () {
    $repo = new DocumentTypeRepository();
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Initial content for Invoice'
    ];

    $documentType = $repo->create($data);

    $updateData = [
        'name'         => 'Updated Invoice',
        'pdf_template' => 'Updated content for Invoice'
    ];
    $updatedDocumentType = $repo->update($documentType, $updateData);

    expect($updatedDocumentType->name)
        ->toBe($updateData['name'])
        ->and($updatedDocumentType->pdf_template)
        ->toBe($updateData['pdf_template']);
});
/**
 * UPDATE using ID
 */
it('updates a document type using ID', function () {
    $repo = new DocumentTypeRepository();
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Initial content for Invoice'
    ];

    $documentType = $repo->create($data);

    $updateData = [
        'name'         => 'Updated Invoice',
        'pdf_template' => 'Updated content for Invoice'
    ];
    $updatedDocumentType = $repo->update($documentType->id, $updateData);

    expect($updatedDocumentType->name)
        ->toBe($updateData['name'])
        ->and($updatedDocumentType->pdf_template)
        ->toBe($updateData['pdf_template']);
});
/**
 * DELETE using Model instance
 */
it('deletes a document type using Model instance', function () {
    $repo = new DocumentTypeRepository();
    $data = [
        'name'         => 'Invoice',
        'pdf_template' => 'Template content for Invoice'
    ];

    $documentType = $repo->create($data);

    $repo->delete($documentType);

    $retrievedDocumentType = $repo->findById($documentType->id);

    expect($retrievedDocumentType)->toBeNull();
});
/**
 * DELETE using ID
 */
it('deletes a document type using ID', function () {
    $repo = new DocumentTypeRepository();
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice'
    ];

    $documentType = $repo->create($data);

    $repo->delete($documentType->id);

    $retrievedDocumentType = $repo->findById($documentType->id);

    expect($retrievedDocumentType)->toBeNull();
});
