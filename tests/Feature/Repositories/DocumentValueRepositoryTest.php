<?php

use App\Enums\FieldType;
use App\Repositories\{
    DocumentFieldRepository,
    DocumentRepository,
    DocumentTypeRepository,
    DocumentValueRepository
};

use App\Models\{
    DocumentValue,
    Document,
    DocumentField
};

beforeEach(function() {
    // Refresh the database for testing.
    Artisan::call('migrate:fresh', ['--env' => 'testing']);

    // Setup a new instance of the repository.
    $this->repo = new DocumentValueRepository();
    $this->repoDocumentType = new DocumentTypeRepository();
    $this->repoDocument = new DocumentRepository();
    $this->repoDocumentField = new DocumentFieldRepository();

    // Create a document type for testing purposes.
    $data = [
        'name'         => 'NewInvoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields'       => [
            [
                'field_name' => 'Total',
                'field_type' => 'number'
            ]
        ]
    ];
    $this->documentType = $this->repoDocumentType->create($data);


    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name' => 'Total',
        'field_type' => FieldType::NUMBER
    ];

    $this->documentField = $this->repoDocumentField->create($data);

    $this->document = $this->repoDocument->create([
        'document_type_id' => $this->documentType->id
    ]);
});

// Test the creation functionality of the repository.
it('can create a document value', function() {
    // Define the test data for creating a document value.
    $data = [
        'document_id'       => $this->document->id,
        'document_field_id' => $this->documentType->fields->first()->id,
        'value'             => 1
    ];

    // Create a new document value.
    $documentValue = $this->repo->create($data);

    // Assert that the created document value matches expectations.
    expect($documentValue)
        ->toBeInstanceOf(DocumentValue::class)
        ->and($documentValue->value)
            ->toEqual(1);
});

// Test the update functionality of the repository.
it('can update a document value', function() {
    // Create a document value for testing.
    $documentValue = DocumentValue::factory()->create(['value' => 'Old Value']);

    // Update the document value.
    $updated = $this->repo->update($documentValue->id, ['value' => 'New Value']);

    // Assert that the updated value matches expectations.
    expect($updated->value)->toEqual('New Value');
});

// Test the ability of the repository to find a document value by its ID.
it('can find a document value by id', function() {
    // Create a document value for testing.
    $documentValue = DocumentValue::factory()->create();

    // Fetch the document value by its ID.
    $found = $this->repo->findById($documentValue->id);

    // Assert that the fetched document value matches expectations.
    expect($found)
            ->toBeInstanceOf(DocumentValue::class)
        ->and($found->id)
            ->toEqual($documentValue->id);
});

// Test the delete functionality of the repository.
it('can delete a document value', function() {
    // Create a document value for testing.
    $documentValue = DocumentValue::factory()->create();

    // Delete the document value.
    // Assert that the document value no longer exists in the database.
    expect($this->repo->delete($documentValue->id))
            ->toBeTrue()
        ->and(DocumentValue::find($documentValue->id))
            ->toBeNull();
});

// Test the retrieval of all document values.
it('can retrieve all document values', function() {
    // Seed the database with multiple document values.
    DocumentValue::factory()->count(5)->create();

    // Fetch all document values.
    $documentValues = $this->repo->getAll();

    // Assert that the correct number of document values were fetched.
    expect($documentValues)->toHaveCount(5);
});

// Test the association between a document value and its document.
it('can retrieve the associated document', function() {
    // Create a document value for a specific document.
    $documentValue = DocumentValue::factory()->create(['document_id' => $this->document->id]);

    // Fetch the associated document.
    $associatedDocument = $this->repo->getDocument($documentValue->id)->first();

    // Assert that the fetched document matches expectations.
    expect($associatedDocument)
        ->toBeInstanceOf(Document::class)
        ->and($associatedDocument->id)
        ->toEqual($this->document->id);
});

// Test the association between a document value and its field.
it('can retrieve the associated document field', function() {
    // Create a document value for a specific field.
    $documentField = DocumentField::factory()->create();
    $documentValue = DocumentValue::factory()->create(['document_field_id' => $documentField->id]);

    // Fetch the associated document field.
    $associatedDocumentField = $this->repo->getDocumentField($documentValue->id)->first();

    // Assert that the fetched field matches expectations.
    expect($associatedDocumentField)
        ->toBeInstanceOf(DocumentField::class)
        ->and($associatedDocumentField->id)
        ->toEqual($documentField->id);
});
