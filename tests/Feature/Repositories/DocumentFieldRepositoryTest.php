<?php

use App\Enums\FieldType;
use App\Models\DocumentField;
use App\Models\DocumentType;
use App\Repositories\DocumentFieldRepository;
use App\Repositories\DocumentTypeRepository;

beforeEach(function () {
    Artisan::call('migrate', ['--env' => 'testing']);

    $repo = new DocumentTypeRepository();
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice'
    ];

    $this->documentType = $repo->create($data);
});

/**
 * Test the creation of a document field.
 */
it('creates and retrieves a document field', function () {
    $docType = new DocumentTypeRepository();

    $repo = new DocumentFieldRepository();
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name' => 'Invoice Number',
        'field_type' => FieldType::STRING
    ];

    $documentField = $repo->create($data);

    expect($documentField)
        ->toBeInstanceOf(DocumentField::class)
        ->and($documentField->field_name)
        ->toBe($data['field_name'])
        ->and($documentField->field_type)
        ->toBe(FieldType::STRING);

    $retrievedDocumentField = $repo->findById($documentField->id);

    expect($retrievedDocumentField->field_name)
        ->toBe($data['field_name'])
        ->and($retrievedDocumentField->field_type)
        ->toBe($data['field_type']);
});

/**
 * Test the updating of a document field.
 */
it('updates a document field', function () {
    $repo = new DocumentFieldRepository();
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number',
        'field_type'       => FieldType::STRING
    ];

    $documentField = $repo->create($data);

    $updateData = [
        'field_name' => 'Updated Invoice Number',
        'field_type' => FieldType::DATE
    ];

    $updatedDocumentField = $repo->update($documentField, $updateData);

    expect($updatedDocumentField->field_name)
        ->toBe($updateData['field_name'])
        ->and($updatedDocumentField->field_type)
        ->toBe($updateData['field_type']);
});

/**
 * Test the deletion of a document field.
 */
it('deletes a document field', function () {
    $repo = new DocumentFieldRepository();
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number',
        'field_type'       => FieldType::STRING
    ];

    $documentField = $repo->create($data);

    $repo->delete($documentField);

    $retrievedDocumentField = $repo->findById($documentField->id);

    expect($retrievedDocumentField)->toBeNull();
});

/**
 * Test the retrieval of all document fields.
 */
it('retrieves all document fields', closure: function () {
    $repo = new DocumentFieldRepository();

    $data1 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number1',
        'field_type'       => FieldType::STRING
    ];
    $data2 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number2',
        'field_type'       => FieldType::DATE
    ];

    $repo->create($data1);
    $repo->create($data2);

    $allDocumentFields = $repo->getAll();

    expect($allDocumentFields)->toHaveCount(2);
});

/**
 * Test the retrieval of document fields by a specific document type.
 */
it('retrieves document fields by document type', function () {
    $repo = new DocumentFieldRepository();

    $data1 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number1',
        'field_type'       => FieldType::STRING
    ];
    $data2 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number2',
        'field_type'       => FieldType::DATE
    ];

    $repo->create($data1);
    $repo->create($data2);

    $documentFieldsByType = $repo->getByDocumentType($this->documentType);

    expect($documentFieldsByType)->toHaveCount(2);
});
/**
 * Test the retrieval of a document field by field name and associated DocumentType ID.
 */
it('retrieves a document field by name and document type ID', function () {
    $repo = new DocumentFieldRepository();
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number',
        'field_type'       => FieldType::STRING
    ];

    $documentField = $repo->create($data);

    $retrievedDocumentField = $repo->findByFieldNameAndTypeId($data['field_name'], $this->documentType->id);

    expect($retrievedDocumentField->id)->toBe($documentField->id);
});

/**
 * Test the deletion of multiple document fields by IDs.
 */
it('deletes multiple document fields by their IDs', function () {
    $repo = new DocumentFieldRepository();

    $data1 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number1',
        'field_type'       => FieldType::STRING
    ];
    $data2 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number2',
        'field_type'       => FieldType::DATE
    ];

    $documentField1 = $repo->create($data1);
    $documentField2 = $repo->create($data2);

    $result = $repo->deleteByIds([$documentField1->id, $documentField2->id]);

    expect($result)
            ->toBeTrue()
        ->and($repo->findById($documentField1->id))
            ->toBeNull()
        ->and($repo->findById($documentField2->id))
            ->toBeNull();
});

/**
 * Test the deletion of all document fields related to a specific DocumentType.
 */
it('deletes all document fields for a given document type', function () {
    $repo = new DocumentFieldRepository();

    $data1 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number1',
        'field_type'       => FieldType::STRING
    ];
    $data2 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number2',
        'field_type'       => FieldType::DATE
    ];

    $documentField1 = $repo->create($data1);
    $documentField2 = $repo->create($data2);

    $result = $repo->deleteByDocumentType($this->documentType);

    expect($result)
            ->toBeTrue()
        ->and($repo->findById($documentField1->id))
            ->toBeNull()
        ->and($repo->findById($documentField2->id))
            ->toBeNull();
});
