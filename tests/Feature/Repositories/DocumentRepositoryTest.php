<?php

use App\Models\Document;
use App\Models\DocumentType;
use App\Repositories\DocumentRepository;
use App\Repositories\DocumentTypeRepository;

/**
 * Setup the database state before each test
 */
beforeEach(function () {
    Artisan::call('migrate:fresh', ['--env' => 'testing']);

    $repo = new DocumentTypeRepository();

    $data = [
        'name'         => 'NewInvoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields'       => [
            [
                'field_name' => 'Total',
                'field_type' => 'number'
            ]
        ]
    ];

    $this->documentType = $repo->create($data);
});

// This test will check if a document can be created using the repository
it('can create a document', function () {
    $repo = new DocumentRepository();
    $document = $repo->create([
        'document_type_id' => $this->documentType->id
    ]);

    // Asserting that the document was created and has the expected name
    expect($document)
            ->toBeInstanceOf(Document::class)
        ->and($document->document_type_id)
            ->toBe($this->documentType->id);
});

// This test checks the update functionality of the repository
it('can update a document', function () {
    $repo = new DocumentRepository();
    $document = $repo->create([
        'document_type_id' => $this->documentType->id
    ]);

    // Assert that the document's name was updated
    expect($document->document_type_id)
        ->toBe($this->documentType->id);

    $repoDocumentType = new DocumentTypeRepository();
    $data = [
        'name'         => 'Invoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields'       => [
            [
                'field_name' => 'Total',
                'field_type' => 'number'
            ]
        ]
    ];

    $documentType = $repoDocumentType->create($data);

    $updatedDocument = $repo->update($document, [
        'document_type_id' => $documentType->id
    ]);

    // Assert that the document's name was updated
    expect($updatedDocument->document_type_id)
        ->toBe($documentType->id);
});

// Test the find by id method
it('can find a document by id', function () {
    $repo = new DocumentRepository();
    $document = $repo->create([
        'document_type_id' => $this->documentType->id
    ]);

    $foundDocument = $repo->findById($document->id);

    // Assert that the correct document was retrieved
    expect($foundDocument->id)
        ->toBe($document->id);
});

// Test the delete method
it('can delete a document', function () {
    $repo = new DocumentRepository();
    $document = $repo->create([
        'document_type_id' => $this->documentType->id
    ]);

    $deleted = $repo->delete($document);

    // Assert the document was deleted
    expect($deleted)
            ->toBeTrue()
        ->and(Document::find($document->id))
            ->toBeNull();
});

// Test the get all method
it('can retrieve all documents', function () {
    Document::factory()->count(5)->create();
    $repo = new DocumentRepository();

    $documents = $repo->getAll();

    // Assert that all documents were retrieved
    expect($documents)
        ->toHaveCount(5);
});

// This test will check that relation retrieval
it('can get the associated DocumentType', function () {
    $repo = new DocumentRepository();
    $document = $repo->create([
        'document_type_id' => $this->documentType->id
    ]);

    $docType = $repo->getDocumentType($document);

    // Assert that the retrieved relation is of expected type
    expect($docType)
        ->toBeInstanceOf(DocumentType::class);
});
