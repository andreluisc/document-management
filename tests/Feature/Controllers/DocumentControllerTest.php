<?php
use App\Models\Document;
use App\Models\DocumentType;
use App\Enums\FieldType;

use App\Repositories\{
    DocumentFieldRepository,
    DocumentRepository,
    DocumentTypeRepository,
    DocumentValueRepository
};

beforeEach(function () {
    // Refresh the database for testing.
    Artisan::call('migrate:fresh', ['--env' => 'testing']);

    // Setup a new instance of the repository.
    $this->repo              = new DocumentValueRepository();
    $this->repoDocumentType  = new DocumentTypeRepository();
    $this->repoDocument      = new DocumentRepository();
    $this->repoDocumentField = new DocumentFieldRepository();


    // Create a document type for testing purposes.
    $data = [
        'name'         => 'NewInvoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields'       => [
            [
                'field_name' => 'Total',
                'field_type' => 'number'
            ]
        ]
    ];
    $this->documentType = $this->repoDocumentType->create($data);


    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name' => 'Total',
        'field_type' => FieldType::NUMBER
    ];

    $this->documentField = $this->repoDocumentField->create($data);

    $this->document = $this->repoDocument->create([
        'document_type_id' => $this->documentType->id
    ]);

    $data = [
        'document_id'       => $this->document->id,
        'document_field_id' => $this->documentType->fields->first()->id,
        'value'             => 1
    ];

    $this->documentValue = $this->repo->create($data);
});

test('it fetches a list of all documents', function () {
    // Arrange: Set up your conditions

    // Act: Execute the function or request
    $response = $this->getJson(route('documents.index'));

    // Assert: Check the expected outcome
    $response->assertStatus(200)
        ->assertJsonCount(1);
});

test('it successfully stores a document', function () {
    // Arrange: Set up the input data
    $data = [
        'document_type_id' => $this->documentType->id,
        'fields' => [
            [
                'field_id' => $this->documentField->id,
                'value' => 100.50
            ]
        ]
    ];

    // Act: Execute the function or request
    $response = $this->postJson(route('documents.store'), $data);

    // Assert: Check the expected outcome
    $response->assertStatus(201)
        ->assertJson([
            'message' => 'Document created successfully!'
        ]);
});

test('it fetches a specific document by its ID', function () {
    $document = Document::first();

    // Act: Execute the function or request
    $response = $this->getJson(route('documents.show', $document->id));

    // Assert: Check the expected outcome
    $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'id'               => $document->id,
                'document_type_id' => $document->document_type_id,
                "pdf_template"     => $this->documentType->pdf_template,
                "fields"           => [
                    [
                        "field_id" => $this->documentType->fields->first()->id,
                        "value"    => "1"
                    ]
                ]
            ]
        ]);
});

test('it successfully deletes a document by its ID', function () {
    // Act: Execute the function or request
    $response = $this->deleteJson(route('documents.destroy', $this->document->id));

    // Assert: Check the expected outcome
    $response->assertStatus(200)
        ->assertJson([
            'message' => 'Document deleted successfully!'
        ]);

    $this->assertDatabaseMissing('documents', ['id' => $this->document->id]);
});

test('it successfully updates a document', function () {
    // Arrange: Create a new field for the document type
    $newFieldData = [
        'document_type_id' => $this->documentType->id,
        'field_name' => 'Date',
        'field_type' => FieldType::DATE
    ];
    $newField = $this->repoDocumentField->create($newFieldData);

    $data = [
        'document_type_id' => $this->documentType->id,
        'fields' => [
            [
                'field_id' => $this->documentField->id,
                'value' => 200.50
            ],
            [
                'field_id' => $newField->id,
                'value' => now()->format('Y-m-d')
            ]
        ]
    ];

    // Act: Execute the function or request
    $response = $this->putJson(route('documents.update', ['document' => $this->document->id]), $data);

    // Assert: Check the expected outcome
    $response->assertStatus(200)
        ->assertJson(['message' => 'Document updated successfully!']);
});

it('generates pdf for a given document', function () {

    $response = $this->get("/api/documents/{$this->document->id}/pdf");

    $response->assertStatus(200);
    $response->assertHeader('Content-Type', 'application/pdf');
});



