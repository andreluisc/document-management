<?php
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\DocumentType;
use App\Exceptions\UnableToCreateDocumentTypeException;
use App\Exceptions\TransactionFailedException;

use App\Enums\FieldType;

use App\Repositories\{
    DocumentFieldRepository,
    DocumentTypeRepository
};

/**
 * Setup the database state before each test
 */
beforeEach(function () {
    Artisan::call('migrate:fresh', ['--env' => 'testing']);

    $repo = new DocumentTypeRepository();
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice'
    ];

    $this->documentType = $repo->create($data);
});

it('can create a new document type', function () {
    // Sample Data for the Test
    $data = [
        'name'         => 'NewInvoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields'       => [
            [
                'field_name' => 'Total',
                'field_type' => 'number'
            ]
        ]
    ];

    // Make POST Request
    $response = $this->postJson(route('document-types.store'), $data);

    // Assert Response
    $response->assertStatus(201)
        ->assertJson(['message' => 'DocumentType successfully created']);

    // Assert Database Entry
    $this->assertDatabaseHas('document_types', ['name' => 'Invoice']);
});

it('returns an error when fields are missing from the template', function () {
    // Invalid Data for the Test (Field missing from pdf_template)
    $data = [
        'name' => 'UniqueInvoiceName',
        'pdf_template' => 'Template content for Invoice.',
        'fields' => [
            [
                'field_name' => 'Total',
                'field_type' => FieldType::NUMBER
            ]
        ]
    ];

    // Make POST Request
    $response = $this->postJson(route('document-types.store'), $data);

    // Assert Response
    $response->assertStatus(400)
        ->assertJson(['message' => 'Following fields are missing in the template: Total']);
});

/**
 * Test the retrieval of a document field by field name and associated DocumentType ID.
 */
it('retrieves a document field by name and document type ID', function () {
    $repo = new DocumentFieldRepository();
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number',
        'field_type'       => FieldType::STRING
    ];

    $documentField = $repo->create($data);

    $retrievedDocumentField = $repo->findByFieldNameAndTypeId($data['field_name'], $this->documentType->id);

    expect($retrievedDocumentField->id)->toBe($documentField->id);
});

/**
 * Test the deletion of multiple document fields by IDs.
 */
it('deletes multiple document fields by their IDs', function () {
    $repo = new DocumentFieldRepository();

    $data1 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number1',
        'field_type'       => FieldType::STRING
    ];
    $data2 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number2',
        'field_type'       => FieldType::DATE
    ];

    $documentField1 = $repo->create($data1);
    $documentField2 = $repo->create($data2);

    $result = $repo->deleteByIds([$documentField1->id, $documentField2->id]);

    expect($result)
            ->toBeTrue()
        ->and($repo->findById($documentField1->id))
            ->toBeNull()
        ->and($repo->findById($documentField2->id))
            ->toBeNull();
});

/**
 * Test the deletion of all document fields related to a specific DocumentType.
 */
it('deletes all document fields for a given document type', function () {
    $repo = new DocumentFieldRepository();

    $data1 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number1',
        'field_type'       => FieldType::STRING
    ];
    $data2 = [
        'document_type_id' => $this->documentType->id,
        'field_name'       => 'Invoice Number2',
        'field_type'       => FieldType::DATE
    ];

    $documentField1 = $repo->create($data1);
    $documentField2 = $repo->create($data2);

    $result = $repo->deleteByDocumentType($this->documentType);

    expect($result)
            ->toBeTrue()
        ->and($repo->findById($documentField1->id))
            ->toBeNull()
        ->and($repo->findById($documentField2->id))
            ->toBeNull();
});
