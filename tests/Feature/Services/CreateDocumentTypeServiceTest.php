<?php

use App\Enums\FieldType;
use App\Services\CreateDocumentTypeService;
use App\Models\DocumentType;
use App\Exceptions\{
    UnableToCreateDocumentTypeException,
    TransactionFailedException
};
use App\Repositories\{
    DocumentTypeRepository,
    DocumentFieldRepository
};

beforeEach(function () {
    Artisan::call('migrate', ['--env' => 'testing']);
});

it('creates a document type and its associated fields successfully', function () {
    $service = new CreateDocumentTypeService();

    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Invoice Number}, {Total}',
        'fields' => [
            [
                'field_name' => 'Invoice Number',
                'field_type' => FieldType::STRING
            ],
            [
                'field_name' => 'Total',
                'field_type' => FieldType::NUMBER
            ]
        ]
    ];

    $documentType = $service->create($data);

    expect($documentType)->toBeInstanceOf(DocumentType::class);
});

it('throws an exception when fields are not present in the template', function () {
    $service = new CreateDocumentTypeService();

    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields' => [
            ['field_name' => 'Invoice Number', 'field_type' => 'string']
        ]
    ];

    $thrownException = null;

    try {
        $service->create($data);
    } catch (UnableToCreateDocumentTypeException $e) {
        $thrownException = $e;
    }

    expect($thrownException)
        ->toBeInstanceOf(UnableToCreateDocumentTypeException::class)
        ->And($thrownException->getMessage())
        ->toContain('Following fields are missing in the template');

});

it('throws a transaction exception on failure', function () {
    $service = new CreateDocumentTypeService();

    $data = [
        'wrong_column_name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields' => [
            [
                'field_name' => 'Total',
                'field_type' => FieldType::NUMBER
            ]
        ]
    ];

    expect(fn() => $service->create($data))->toThrow(TransactionFailedException::class);
});
