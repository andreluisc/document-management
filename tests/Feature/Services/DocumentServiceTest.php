<?php

use App\Models\Document;
use App\Services\DocumentService;
use App\Enums\FieldType;
use App\Models\DocumentType;
use App\Models\DocumentField;

beforeEach(function() {
    // Refresh the database for testing.
    Artisan::call('migrate:fresh', ['--env' => 'testing']);

    // Instantiate our service
    $this->service = app(DocumentService::class);

    // Set up some test data for use in our tests.
    $this->documentType = DocumentType::factory()->create([
        'name' => 'NewInvoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}'
    ]);

    $this->documentField = DocumentField::factory()->create([
        'document_type_id' => $this->documentType->id,
        'field_name' => 'Total',
        'field_type' => FieldType::NUMBER
    ]);
});

it('creates a document with values', function() {
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_values' => [
            $this->documentField->id => 100
        ]
    ];

    $document = $this->service->createDocument($data);

    // Assert that a document was created and it has the expected type.
    expect($document)
        ->toBeInstanceOf(Document::class)
        ->and($document->document_type_id)
        ->toEqual($this->documentType->id);

    // Assert that the document value was created and stored correctly.
    $documentValue = $document->documentValues->first();

    expect($documentValue->document_field_id)
        ->toEqual($this->documentField->id)
        ->and($documentValue->value)
        ->toEqual('100');
});

it('generates a pdf from a document', function() {
    $data = [
        'document_type_id' => $this->documentType->id,
        'field_values' => [
            $this->documentField->id => 100
        ]
    ];

    $document = $this->service->createDocument($data);

    $pdfContent = $this->service->generatePdf($document->id);

    // Basic check. This will only confirm the PDF was generated.
    expect(strlen($pdfContent))->toBeGreaterThan(0);
});
