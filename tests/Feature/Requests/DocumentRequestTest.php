<?php
use App\Models\DocumentType;
use App\Enums\FieldType;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

beforeEach(function() {
    // Refresh the database for testing.
    Artisan::call('migrate:fresh', ['--env' => 'testing']);

    // Setup a new instance of the repository.
    $this->repo = new DocumentValueRepository();
    $this->repoDocumentType = new DocumentTypeRepository();
    $this->repoDocument = new DocumentRepository();
    $this->repoDocumentField = new DocumentFieldRepository();

    // Create a document type for testing purposes.
    $data = [
        'name'         => 'NewInvoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields'       => [
            [
                'field_name' => 'Total',
                'field_type' => 'number'
            ]
        ]
    ];
    $this->documentType = $this->repoDocumentType->create($data);


    $data = [
        'document_type_id' => $this->documentType->id,
        'field_name' => 'Total',
        'field_type' => FieldType::NUMBER
    ];

    $this->documentField = $this->repoDocumentField->create($data);

    $this->document = $this->repoDocument->create([
        'document_type_id' => $this->documentType->id
    ]);
});

it('validates document_type_id exists', function() {
    $response = $this->postJson(route('documents.store'), [
        'document_type_id' => 999, // non-existent ID
        'field_values' => [
            $this->documentField->id => '100'
        ]
    ]);

    $response->assertStatus(422)
        ->assertJsonValidationErrors('document_type_id');
});

it('validates field_values based on FieldType', function() {
    // Valid input
    $response = $this->postJson(route('documents.store'), [
        'document_type_id' => $this->documentType->id,
        'field_values' => [
            $this->documentField->id => '100'
        ]
    ]);
    $response->assertStatus(201); // assuming 201 is the created status

    // Invalid input (number expected, string given)
    $response = $this->postJson(route('documents.store'), [
        'document_type_id' => $this->documentType->id,
        'field_values' => [
            $this->documentField->id => 'not a number'
        ]
    ]);
    $response->assertStatus(422)
        ->assertJsonValidationErrors('field_values.' . $this->documentField->id);
});

it('ensures required fields are provided', function() {
    $response = $this->postJson(route('documents.store'), [
        'document_type_id' => $this->documentType->id,
        // not providing field_values
    ]);

    $response->assertStatus(422)
        ->assertJsonValidationErrors('field_values');
});
