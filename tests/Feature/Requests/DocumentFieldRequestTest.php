<?php

use App\Http\Requests\DocumentFieldRequest;
use App\Enums\FieldType;
use App\Models\DocumentType;
use App\Repositories\DocumentTypeRepository;
use Illuminate\Support\Facades\Validator;

it('validates required fields', closure: function () {
    $data = [
        // 'name' is missing
        'pdf_template' => 'Sample PDF Template',
        'fields'       => [
            [
                'field_name' => 'total',
                'field_type' => 'number'
            ],
        ]
    ];

    $request = new DocumentFieldRequest();
    $rules = $request->rules();
    $validator = Validator::make($data, $rules);

    expect($validator->fails())
        ->toBeTrue()
        ->and($validator->errors()->get('name'))
        ->toContain('The name field is required.');
});

it('validates field types', function () {
    $data = [
        'name' => 'Invoice',
        'pdf_template' => 'Template content for Invoice. Fields: {Total}',
        'fields' => [
            [
                'field_name' => 'Total',
                'field_type' => 'not-a-valid-type'
            ]
        ]
    ];

    $request = new DocumentFieldRequest();
    $request->merge($data); // This ensures that the request object has the provided data

    $rules = $request->rules();
    $validator = Validator::make($data, $rules);

    if (!$validator->fails()) {
        dd($data, $rules, $validator->errors());
    }

    expect($validator->fails())->toBeTrue()
        ->and($validator->errors()->get('fields.0.field_type'))
        ->toContain('The selected fields.0.field type is invalid.');
});


it('passes with valid data', function () {
    $data = [
        'name'         => 'Invoice',
        'pdf_template' => 'Sample PDF Template with {total}',
        'fields' => [
            [
                'field_name' => 'total',
                'field_type' => 'number'
            ],
            [
                'field_name' => 'invoiceDate',
                'field_type' => 'date'
            ]
        ]
    ];

    $request = new DocumentFieldRequest();
    $rules = $request->rules();
    $validator = Validator::make($data, $rules);

    expect($validator->fails())->toBeFalse();
});

it('fails when name is not unique for creation', closure: function () {
    // Create a record first
    $docTypeRepo = new DocumentTypeRepository();
    $docTypeRepo->create(['name' => 'Invoice', 'pdf_template' => 'Some template']);

    $data = [
        'name' => 'Invoice',  // Using the same name
        'pdf_template' => 'Another template',
        'fields' => [
            [
                'field_name' => 'total',
                'field_type' => FieldType::NUMBER
            ]
        ]
    ];

    $request = new DocumentFieldRequest();
    $rules = $request->rules();
    $validator = Validator::make($data, $rules);

    expect($validator->fails())->toBeTrue()
        ->and($validator->errors()->get('name'))
        ->toContain('The name has already been taken.');
});

it('passes when name is not unique but it\'s an update for the same record', function () {
    // Create a record first
    $docTypeRepo = new DocumentTypeRepository();
    $documentType = $docTypeRepo->create(['name' => 'Invoice', 'pdf_template' => 'Some template']);

    $data = [
        'name' => 'Invoice',  // Using the same name
        'pdf_template' => 'Updated template',
        'fields' => [
            [
                'field_name' => 'total',
                'field_type' => FieldType::NUMBER
            ]
        ]
    ];

    $request = new DocumentFieldRequest();

    // Simulate route param
    $request->setRouteResolver(fn() => new class($documentType->id) {
        private $id;
        public function __construct($id) {
            $this->id = $id;
        }
        public function parameter($name) {
            return $this->id;
        }
    });

    $rules = $request->rules();
    $validator = Validator::make($data, $rules);

    expect($validator->fails())->toBeFalse();
});
