<?php

use App\Models\{
    DocumentType,
    DocumentField
};

it('belongs to a document type', function () {
    $documentType = DocumentType::factory()->create();
    $documentField = DocumentField::factory()->create(['document_type_id' => $documentType->id]);

    expect($documentField->documentType->attributesToArray())->toEqual($documentType->attributesToArray());
});

it('can be created', function () {
    $documentField = DocumentField::factory()->create();

    expect($documentField)
        ->toBeInstanceOf(DocumentField::class)
        ->and(DocumentField::find($documentField->id)->attributesToArray())
        ->toEqual($documentField->attributesToArray());

});
