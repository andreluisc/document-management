<?php

use App\Models\{
    DocumentType,
    DocumentField
};

it('has many document fields', function () {
    $documentType = DocumentType::factory()->create();

    DocumentField::factory()->count(3)->create([
        'document_type_id' => $documentType->id
    ]);

    expect($documentType->fields)->toHaveCount(3);
});

it('can be created', function () {
    $documentType = DocumentType::factory()->create();

    expect($documentType)
        ->toBeInstanceOf(DocumentType::class)
        ->and(DocumentType::find($documentType->id)->attributesToArray())
        ->toEqual($documentType->attributesToArray());
});
