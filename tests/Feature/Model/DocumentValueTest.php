<?php

use App\Models\{
    Document,
    DocumentField,
    DocumentValue
};

it('belongs to a document', function () {
    $document = Document::factory()->create();
    $documentValue = DocumentValue::factory()->create(['document_id' => $document->id]);

    expect($documentValue->document->attributesToArray())
        ->toEqual($document->attributesToArray());
});

it('belongs to a document field', function () {
    $documentField = DocumentField::factory()->create();
    $documentValue = DocumentValue::factory()->create(['document_field_id' => $documentField->id]);

    expect($documentValue->documentField->attributesToArray())
        ->toEqual($documentField->attributesToArray());
});

it('can be created', function () {
    $documentValue = DocumentValue::factory()->create();

    expect($documentValue)
        ->toBeInstanceOf(DocumentValue::class)
        ->and(DocumentValue::find($documentValue->id)->attributesToArray())
        ->toEqual($documentValue->attributesToArray());
});
