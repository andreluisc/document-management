<?php

use App\Models\{
    Document,
    DocumentType
};

it('belongs to a document type', function () {
    $documentType = DocumentType::factory()->create();
    $document = Document::factory()->create(['document_type_id' => $documentType->id]);

    expect($document->documentType->attributesToArray())
        ->toEqual($documentType->attributesToArray());
});

it('can be created', function () {
    $document = Document::factory()->create();

    expect($document)
        ->toBeInstanceOf(Document::class)
        ->and(Document::find($document->id)->attributesToArray())
        ->toEqual($document->attributesToArray());
});
