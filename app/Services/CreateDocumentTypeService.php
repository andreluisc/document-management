<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

use App\Repositories\{
    DocumentTypeRepository,
    DocumentFieldRepository
};

use App\Models\DocumentType;

use App\Exceptions\{
    UnableToCreateDocumentTypeException,
    TransactionFailedException
};

class CreateDocumentTypeService
{
    protected DocumentTypeRepository $documentTypeRepo;
    protected DocumentFieldRepository $documentFieldRepo;

    public function __construct() {
        $this->documentTypeRepo = new DocumentTypeRepository();
        $this->documentFieldRepo = new DocumentFieldRepository();
    }

    /**
     * Handle the creation of a DocumentType and its associated fields.
     *
     * @param array $data Data for DocumentType and its fields.
     * @return DocumentType The created DocumentType instance.
     * @throws TransactionFailedException If the database transaction fails.
     */
    public function create(array $data): DocumentType
    {
        /**
         * @todo Laravel transaction is causing error `1305 SAVEPOINT trans2 does not exist`
         */
//        \Log::info('Transaction Depth: ' . DB::transactionLevel());
//        DB::beginTransaction();

        try {
            // Validate if all fields are present in the template
            $this->validateFieldsInTemplate($data['fields'], $data['pdf_template']);

            // Save the document type
            $documentType = $this->documentTypeRepo->create([
                'name'         => $data['name'],
                'pdf_template' => $data['pdf_template'],
            ]);

            // Save the document fields
            foreach ($data['fields'] as $field) {
                $this->documentFieldRepo->create([
                    'document_type_id' => $documentType->id,
                    'field_name'       => $field['field_name'],
                    'field_type'       => $field['field_type'],
                ]);
            }
//            DB::commit();
            return $documentType;
        } catch (\Exception $e) {
//            DB::rollBack();
            if ($e instanceof UnableToCreateDocumentTypeException) {
                throw $e; // re-throw the original exception
            }
            throw new TransactionFailedException('Unable to save the document type and its fields.');
        }
    }

    /**
     * Validate that all provided fields are present in the template content.
     *
     * @param array $fields Fields to validate.
     * @param string $template Template content.
     * @throws UnableToCreateDocumentTypeException If any field is missing in the template.
     */
    protected function validateFieldsInTemplate(array $fields, string $template): void
    {

        $missingFields = Collection::make($fields)
            ->filter(fn ($field) => !str_contains($template, '{'.$field['field_name'].'}'))
            ->pluck('field_name')
            ->toArray();
        if(!empty($missingFields)) {
            throw new UnableToCreateDocumentTypeException(
                'Following fields are missing in the template: ' . implode(', ', $missingFields)
            );
        }
    }
}

