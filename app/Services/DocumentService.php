<?php

namespace App\Services;

use App\Models\Document;
use App\Repositories\{
    DocumentValueRepository,
    DocumentTypeRepository,
    DocumentRepository,
    DocumentFieldRepository
};
use Dompdf\Dompdf;
use Dompdf\Options;

class DocumentService
{
    protected DocumentRepository      $documentRepo;
    protected DocumentValueRepository $documentValueRepo;
    protected DocumentTypeRepository  $documentTypeRepo;
    protected DocumentFieldRepository $documentFieldRepo;

    public function __construct() {
        $this->documentRepo = new DocumentRepository();
        $this->documentValueRepo = new DocumentValueRepository();
        $this->documentTypeRepo = new DocumentTypeRepository();
        $this->documentFieldRepo = new DocumentFieldRepository();
    }

    public function createDocument(array $data): Document
    {
        $document = $this->documentRepo->create(['document_type_id' => $data['document_type_id']]);

        foreach ($data['fields'] as $key => $field) {
            $this->documentValueRepo->create([
                'document_id'       => $document->id,
                'document_field_id' => $field['field_id'],
                'value'             => $field['value']
            ]);
        }

        return $document;
    }

    public function updateDocument(int $id, array $data): Document
    {
        $document = $this->documentRepo->update($id, ['document_type_id' => $data['document_type_id']]);

        $this->documentValueRepo->delete($document->id);

        foreach ($data['fields'] as $field) {
            $this->documentValueRepo->create([
                'document_id'       => $document->id,
                'document_field_id' => $field['field_id'],
                'value'             => $field['value']
            ]);
        }

        return $document;
    }


    public function generatePdf(int $documentId): string
    {
        $document = $this->documentRepo->findById($documentId);
        $documentType = $this->documentTypeRepo->findById($document->document_type_id);
        $template = $documentType->pdf_template;

        $documentValues = $this->documentValueRepo->getDocument($documentId);

        foreach ($documentValues as $docValue) {
            $field = $this->documentFieldRepo->findById($docValue->document_field_id);
            $template = str_replace("{" . $field->field_name . "}", $docValue->value, $template);
        }

        $dompdfOptions = new Options();
        $dompdfOptions->set('isHtml5ParserEnabled', true);

        $dompdf = new Dompdf($dompdfOptions);
        $dompdf->loadHtml($template);
        $dompdf->render();

        return $dompdf->output();
    }
}
