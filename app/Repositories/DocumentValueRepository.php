<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\Models\DocumentValue;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DocumentValueRepository
{
    protected DocumentValue $model;

    public function __construct()
    {
        $this->model = new DocumentValue();
    }

    /**
     * Create a new DocumentValue with given attributes.
     */
    public function create(array $attributes): DocumentValue
    {
        return $this->model->create($attributes);
    }

    /**
     * Update an existing DocumentValue with given attributes.
     */
    public function update($documentValueOrId, array $attributes): DocumentValue
    {
        $documentValue = $this->resolveModel($documentValueOrId);
        $documentValue->update($attributes);
        return $documentValue;
    }

    /**
     * Find a DocumentValue by its ID.
     */
    public function findById($id): ?DocumentValue
    {
        return $this->model->find($id);
    }

    /**
     * Delete a DocumentValue by its ID or model.
     */
    public function delete($documentValueOrId): bool
    {
        $documentValue = $this->resolveModel($documentValueOrId);
        if ($documentValue) {
            return $documentValue->delete();
        }
        return false;
    }

    /**
     * Get all DocumentValues.
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * Get the Document associated with a DocumentValue.
     */
    public function getDocument($documentValueOrId): BelongsTo
    {
        $documentValue = $this->resolveModel($documentValueOrId);
        return $documentValue->document();
    }

    /**
     * Get the DocumentField associated with a DocumentValue.
     */
    public function getDocumentField($documentValueOrId): BelongsTo
    {
        $documentValue = $this->resolveModel($documentValueOrId);
        return $documentValue->documentField();
    }

    /**
     * Resolve the input to an instance of DocumentValue.
     */
    private function resolveModel($documentValueOrId): ?DocumentValue
    {
        if ($documentValueOrId instanceof DocumentValue) {
            return $documentValueOrId;
        }
        return $this->findById($documentValueOrId);
    }
}
