<?php

namespace App\Repositories;

use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Collection;

class DocumentTypeRepository
{
    protected DocumentType $model;

    public function __construct()
    {
        $this->model = new DocumentType();
    }

    /**
     * Create a new DocumentType with given attributes.
     */
    public function create(array $attributes): DocumentType
    {
        return $this->model->create($attributes);
    }

    /**
     * Update an existing DocumentType with given attributes.
     */
    public function update($documentTypeOrId, array $attributes): DocumentType
    {
        $documentType = $this->resolveModel($documentTypeOrId);
        $documentType->update($attributes);
        return $documentType->refresh();
    }

    /**
     * Find a DocumentType by its ID.
     */
    public function findById($id): ?DocumentType
    {
        return $this->model->find($id);
    }

    /**
     * Delete a DocumentType by its ID or model.
     */
    public function delete($documentTypeOrId): bool
    {
        $documentType = $this->resolveModel($documentTypeOrId);
        if ($documentType) {
            return $documentType->delete();
        }
        return false;
    }

    /**
     * Get all DocumentTypes.
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * Get all fields associated with a DocumentType.
     */
    public function getFields($documentTypeOrId): Collection
    {
        $documentType = $this->resolveModel($documentTypeOrId);
        if ($documentType) {
            return $documentType->fields;
        }
        return new Collection();
    }

    /**
     * Resolve the input to an instance of DocumentType.
     */
    private function resolveModel($documentTypeOrId): ?DocumentType
    {
        if ($documentTypeOrId instanceof DocumentType) {
            return $documentTypeOrId;
        }
        return $this->findById($documentTypeOrId);
    }
}
