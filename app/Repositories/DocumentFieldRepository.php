<?php

namespace App\Repositories;

use App\Models\DocumentField;
use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Collection;

class DocumentFieldRepository
{
    protected DocumentField $model;

    public function __construct()
    {
        $this->model = new DocumentField();
    }

    /**
     * Create a new DocumentField with given attributes.
     */
    public function create(array $attributes): DocumentField
    {
        return $this->model->create($attributes);
    }

    /**
     * Update an existing DocumentField with given attributes.
     */
    public function update($documentFieldOrId, array $attributes): DocumentField
    {
        $documentField = $this->resolveModel($documentFieldOrId);
        $documentField->update($attributes);
        return $documentField->refresh();
    }

    /**
     * Find a DocumentField by its ID.
     */
    public function findById($id): ?DocumentField
    {
        return $this->model->find($id);
    }

    /**
     * Delete a DocumentField by its ID or model.
     */
    public function delete($documentFieldOrId): bool
    {
        $documentField = $this->resolveModel($documentFieldOrId);
        if ($documentField) {
            return $documentField->delete();
        }
        return false;
    }

    /**
     * Retrieve all DocumentFields.
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * Retrieve DocumentFields by an associated DocumentType.
     */
    public function getByDocumentType(DocumentType $documentType): Collection
    {
        return $documentType->fields;
    }

    /**
     * Retrieve a DocumentField by its name and associated DocumentType ID.
     */
    public function findByFieldNameAndTypeId(string $fieldName, int $typeId): ?DocumentField
    {
        return $this->model->where('field_name', $fieldName)
            ->where('document_type_id', $typeId)
            ->first();
    }

    /**
     * Delete DocumentFields by an array of IDs.
     */
    public function deleteByIds(array $ids): bool
    {
        return $this->model->whereIn('id', $ids)->delete();
    }

    /**
     * Delete all DocumentFields related to a specific DocumentType.
     */
    public function deleteByDocumentType(DocumentType $documentType): bool
    {
        return $this->model->where('document_type_id', $documentType->id)->delete();
    }

    /**
     * Resolve the input to an instance of DocumentField.
     */
    private function resolveModel($documentFieldOrId): ?DocumentField
    {
        if ($documentFieldOrId instanceof DocumentField) {
            return $documentFieldOrId;
        }
        return $this->findById($documentFieldOrId);
    }
}
