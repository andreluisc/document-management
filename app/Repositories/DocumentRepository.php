<?php

namespace App\Repositories;

use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Models\Document;

class DocumentRepository
{
    protected Document $model;

    public function __construct()
    {
        $this->model = new Document();
    }


    /**
     * Create a new Document with given attributes.
     */
    public function create(array $attributes): Document
    {
        return $this->model->create($attributes);
    }

    /**
     * Update an existing Document with given attributes.
     */
    public function update($documentOrId, array $attributes): Document
    {
        $document = $this->resolveModel($documentOrId);
        $document->update($attributes);
        return $document->refresh();
    }

    /**
     * Find a Document by its ID.
     */
    public function findById($id): ?Document
    {
        return $this->model->find($id);
    }

    /**
     * Delete a Document by its ID or model.
     */
    public function delete($documentOrId): bool
    {
        $document = $this->resolveModel($documentOrId);
        if ($document) {
            return $document->delete();
        }
        return false;
    }

    /**
     * Get all Documents.
     */
    public function getAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * Get the DocumentType associated with a Document.
     */
    public function getDocumentType($documentOrId): ?DocumentType
    {
        $document = $this->resolveModel($documentOrId);
        return $document?->documentType;
    }

    /**
     * Resolve the input to an instance of Document.
     */
    private function resolveModel($documentOrId): ?Document
    {
        if ($documentOrId instanceof Document) {
            return $documentOrId;
        }
        return $this->findById($documentOrId);
    }
}
