<?php

namespace App\Enums;

enum FieldType: string {
    case STRING = 'string';
    case DATE = 'date';
    case NUMBER = 'number';
    case CURRENCY = 'currency';
}
