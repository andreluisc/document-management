<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'document_type_id' => $this->document_type_id,
            'pdf_template'     => $this->documentType->pdf_template,
            'fields'           => $this->documentValues->map(function ($field) {
                return [
                    'field_id' => $field->document_field_id,
                    'value'    => $field->value,
                ];
            }),
        ];
    }
}
