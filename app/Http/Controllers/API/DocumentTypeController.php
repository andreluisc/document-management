<?php

namespace App\Http\Controllers\API;

use App\Exceptions\TransactionFailedException;
use App\Exceptions\UnableToCreateDocumentTypeException;
use App\Http\Resources\DocumentTypeResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentFieldRequest;

use App\Services\CreateDocumentTypeService;
use App\Repositories\DocumentTypeRepository;

/**
 * @OA\Tag(name="DocumentTypes")
 */
class DocumentTypeController extends Controller
{
    protected CreateDocumentTypeService $createDocumentTypeService;
    protected DocumentTypeRepository $documentTypeRepo;

    public function __construct() {
    }
    /**
     * Retrieve all document types.
     *
     * @return JsonResponse
     */
    /**
     * @OA\Get(
     *      path="/api/document-types",
     *      tags={"DocumentTypes"},
     *      summary="Retrieve all document types.",
     *      @OA\Response(
     *          response=200,
     *          description="List of document types."
     *      )
     * )
     */
    public function index()
    {
        $this->documentTypeRepo = new DocumentTypeRepository();
        $documentTypes = $this->documentTypeRepo->getAll();

        return response()->json(['data' => $documentTypes], 200);
    }

    /**
     * Create a new document type.
     *
     * @param DocumentFieldRequest $request
     * @return JsonResponse
     */
    /**
     * @OA\Post(
     *      path="/api/document-types",
     *      tags={"DocumentTypes"},
     *      summary="Create a new document type.",
     *      @OA\RequestBody(
     *          description="Document type data",
     *          required=true,
     *          @OA\JsonContent(ref="DocumentFieldRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="DocumentType successfully created."
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad request."
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Failed to create DocumentType."
     *      )
     * )
     */
    public function store(DocumentFieldRequest $request)
    {
        $this->createDocumentTypeService = new CreateDocumentTypeService();

        try {

            $documentType = $this->createDocumentTypeService->create($request->validated());

            return response()->json([
                'data'    => $documentType,
                'message' => 'DocumentType successfully created'
            ], 201);

        } catch (UnableToCreateDocumentTypeException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        } catch (TransactionFailedException $e) {
            return response()->json(['message' => 'Failed to create DocumentType'], 500);
        }
    }

    /**
     * Retrieve a specific document type.
     *
     * @param int $id
     * @return DocumentTypeResource
     */
    /**
     * @OA\Get(
     *      path="/api/document-types/{id}",
     *      tags={"DocumentTypes"},
     *      summary="Retrieve a specific document type.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the document type to retrieve.",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Specific document type."
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="DocumentType not found."
     *      )
     * )
     */
    public function show(int $id)
    {
        $this->documentTypeRepo = new DocumentTypeRepository();
        $documentType = $this->documentTypeRepo->findById($id);

        if (!$documentType) {
            return response()->json(['message' => 'DocumentType not found'], 404);
        }

        return new DocumentTypeResource($documentType);
    }

    /**
     * Update a specific document type.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    /**
     * @OA\Put(
     *      path="/api/document-types/{id}",
     *      tags={"DocumentTypes"},
     *      summary="Update a specific document type.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the document type to update.",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\RequestBody(
     *          description="Document type data to update",
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="pdf_template", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="DocumentType successfully updated."
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="DocumentType not found."
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Failed to update DocumentType."
     *      )
     * )
     */
    public function update(Request $request, int $id)
    {
        // Instantiate the DocumentTypeRepository
        $this->documentTypeRepo = new DocumentTypeRepository();

        // Find the DocumentType by its ID
        $documentType = $this->documentTypeRepo->findById($id);

        // If DocumentType not found, return a 404 error
        if (!$documentType) {
            return response()->json(['message' => 'DocumentType not found'], 404);
        }

        // Validate the incoming request data
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'pdf_template' => 'required|string'
        ]);

        try {
            // Update the DocumentType using the Repository
            $updatedDocumentType = $this->documentTypeRepo->update($documentType, $validatedData);

            return response()->json([
                'data' => $updatedDocumentType,
                'message' => 'DocumentType successfully updated'
            ], 200);

        } catch (\Exception $e) {
            // Handle any exception that might occur during the update process
            return response()->json(['message' => 'Failed to update DocumentType', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Delete a specific document type.
     *
     * @param int $id
     * @return JsonResponse
     */
    /**
     * @OA\Delete(
     *      path="/api/document-types/{id}",
     *      tags={"DocumentTypes"},
     *      summary="Delete a specific document type.",
     *      @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="ID of the document type to delete.",
     *          required=true,
     *          @OA\Schema(type="integer")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="DocumentType successfully deleted."
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="DocumentType not found."
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="Failed to delete DocumentType."
     *      )
     * )
     */
    public function destroy(int $id)
    {
        $this->documentTypeRepo = new DocumentTypeRepository();

        $documentType = $this->documentTypeRepo->findById($id);
        if (!$documentType) {
            return response()->json(['message' => 'DocumentType not found'], 404);
        }

        try {
            $this->documentTypeRepo->delete($documentType);

            return response()->json(['message' => 'DocumentType successfully deleted'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to delete DocumentType', 'error' => $e->getMessage()], 500);
        }
    }
}
