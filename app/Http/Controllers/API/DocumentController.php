<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DocumentResource;
use App\Repositories\DocumentRepository;
use App\Services\DocumentService;
use App\Http\Requests\DocumentRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * @OA\Tag(
 *     name="Documents",
 *     description="API Endpoints for managing documents"
 * )
 */
class DocumentController extends Controller
{
    protected DocumentService  $documentService;
    private DocumentRepository $repoDocuments;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->documentService = new DocumentService();
        $this->repoDocuments   = new DocumentRepository();
    }

    /**
     * Display a listing of the documents.
     *
     * @return JsonResponse
     */
    /**
     * @OA\Get(
     *     path="/api/documents",
     *     tags={"Documents"},
     *     summary="Get a list of all documents",
     *     @OA\Response(
     *         response=200,
     *         description="List of documents"
     *     )
     * )
     */
    public function index()
    {
        // Retrieve all documents for API response.
        return response()->json(
            $this->repoDocuments->getAll()
        );
    }

    /**
     * Store a newly created document in storage.
     *
     * @param  DocumentRequest  $request
     * @return JsonResponse
     */
    /**
     * @OA\Post(
     *     path="/api/documents",
     *     tags={"Documents"},
     *     summary="Create a new document",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="DocumentRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Document successfully created"
     *     )
     * )
     */
    public function store(DocumentRequest $request)
    {
        $document = $this->documentService->createDocument($request->validated());

        return response()->json([
            'message' => 'Document created successfully!',
            'data' => $document
        ], 201);
    }

    /**
     * Display the specified document.
     *
     * @param  int  $id
     * @return DocumentResource | JsonResponse
     */
    /**
     * @OA\Get(
     *     path="/api/documents/{id}",
     *     tags={"Documents"},
     *     summary="Get details of a specific document",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the document to fetch",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Details of the document"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Document not found"
     *     )
     * )
     */
    public function show(int $id)
    {
        $document = $this->repoDocuments->findById($id);

        if (!$document) {
            return response()->json(['message' => 'Document not found'], 404);
        }

        return new DocumentResource($document);
    }

    /**
     * Update the specified document in storage.
     *
     * @param  DocumentRequest  $request
     * @param  int  $id
     * @return JsonResponse
     */
    /**
     * @OA\Post(
     *     path="/api/documents/{id}",
     *     tags={"Documents"},
     *     summary="Update a specific document",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the document to update",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="DocumentRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Document successfully updated"
     *     )
     * )
     */
    public function update(DocumentRequest $request, int $id)
    {
        $this->documentService->updateDocument($id, $request->validated());

        return response()->json(['message' => 'Document updated successfully!']);
    }


    /**
     * Remove the specified document from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    /**
     * @OA\Delete(
     *     path="/api/documents/{id}",
     *     tags={"Documents"},
     *     summary="Delete a specific document",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the document to delete",
     *         required=true
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Document successfully deleted"
     *     )
     * )
     */
    public function destroy(int $id)
    {
        $this->repoDocuments->delete($id);

        return response()->json(['message' => 'Document deleted successfully!']);
    }

    /**
     * Generate PDF for the given document ID.
     *
     * @param int $documentId
     * @return Application|ResponseFactory|\Illuminate\Foundation\Application|Response
     *
     * @OA\Get(
     *      path="/documents/{documentId}/pdf",
     *      tags={"Documents"},
     *      summary="Generate a PDF for the given document ID.",
     *      @OA\Parameter(
     *          name="documentId",
     *          in="path",
     *          description="ID of the document",
     *          required=true,
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="PDF generated successfully."
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Document not found."
     *      )
     * )
     */
    public function generatePdf(int $documentId): Application|ResponseFactory|\Illuminate\Foundation\Application|Response
    {
        $pdfContent = $this->documentService->generatePdf($documentId);
        return response($pdfContent, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="document.pdf"'
        ]);
    }
}
