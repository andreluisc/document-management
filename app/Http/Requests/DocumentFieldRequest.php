<?php

namespace App\Http\Requests;

use App\Enums\FieldType;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="DocumentFieldRequest",
 *     type="object",
 *     required={"name", "pdf_template", "fields"},
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="The name of the document type"
 *     ),
 *     @OA\Property(
 *         property="pdf_template",
 *         type="string",
 *         description="The template for the PDF format of the document type"
 *     ),
 *     @OA\Property(
 *         property="fields",
 *         type="array",
 *         description="An array of fields associated with the document type",
 *         @OA\Items(
 *             type="object",
 *             required={"field_name", "field_type"},
 *             @OA\Property(
 *                 property="field_name",
 *                 type="string",
 *                 description="The name of the field"
 *             ),
 *             @OA\Property(
 *                 property="field_type",
 *                 type="string",
 *                 enum={"number", "date", "string", "currency"},
 *                 description="The type of the field"
 *             )
 *         )
 *     )
 * )
 */
class DocumentFieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $fieldsData = $this->input('fields', []);

        // Get all valid field types as string values
        $validFieldTypes = array_map(function ($fieldType) {
            return $fieldType->value;
        }, FieldType::cases());

        $fieldRules = [];

        foreach ($fieldsData as $index => $fieldData) {
            $fieldRules["fields.$index.field_name"] = ['required', 'string'];
            $fieldRules["fields.$index.field_type"] = ['required', 'in:' . implode(',', $validFieldTypes)];
        }

        // For create/store operation
        $nameRule = ['required', 'string', 'unique:document_types,name'];

        // If the request has a "document_type" route parameter indicates of an update
        if ($this->route('document_type')) {
            $nameRule[2] = 'unique:document_types,name,' . $this->route('document_type');
        }

        return array_merge([
            'name' => $nameRule,
            'pdf_template' => 'required|string',
        ], $fieldRules);
    }

}
