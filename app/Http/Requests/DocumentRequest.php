<?php

namespace App\Http\Requests;

use App\Models\DocumentField;
use App\Repositories\DocumentTypeRepository;
use Illuminate\Foundation\Http\FormRequest;
use App\Enums\FieldType;
use App\Models\DocumentType;


/**
 * @OA\Schema(
 *     schema="DocumentRequest",
 *     type="object",
 *     required={"document_type_id", "fields"},
 *     @OA\Property(
 *         property="document_type_id",
 *         type="integer",
 *         description="ID of the associated Document Type",
 *         example=1
 *     ),
 *     @OA\Property(
 *         property="fields",
 *         type="array",
 *         description="Array of field data",
 *         @OA\Items(
 *             type="object",
 *             required={"field_id", "value"},
 *             @OA\Property(
 *                 property="field_id",
 *                 type="integer",
 *                 description="The ID of the field",
 *                 example=5
 *             ),
 *             @OA\Property(
 *                 property="value",
 *                 type="string",
 *                 description="The value of the field. The type/validation depends on the field type (e.g. date, number, currency, string).",
 *                 example="John Doe"
 *             )
 *         )
 *     )
 * )
 */
class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'document_type_id' => [$this->isMethod('post') ? 'required' : 'sometimes', 'exists:' . DocumentType::class . ',id'],
            'fields' => ['required', 'array']
        ];

        // Loop through each provided field value and determine its validation rule based on the type.
        $repoDocType = new DocumentTypeRepository();

        foreach ($this->get('fields', []) as $key => $field) {
            $docType = $repoDocType->findById($this->document_type_id);
            $fields = $repoDocType->getFields($docType);

            $rules["fields.{$key}.field_id"] = 'required|exists:'. DocumentField::class.',id';

            $type = $fields->where('id', $field['field_id'])->first()->field_type ?? null;

            if (!$type) continue; // Skip if field type is not found

            $rules["fields.{$key}.value"] = match($type) {
                FieldType::DATE     => 'required|date',
                FieldType::NUMBER   => 'required|numeric',
                FieldType::CURRENCY => 'required|regex:/^\d+(\.\d{1,2})?$/',
                default             => 'required|string',
            };
        }

        return $rules;
    }
}
