<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\{
    DocumentTypeController,
    DocumentController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::ApiResource('document-types', DocumentTypeController::class);
Route::ApiResource('documents', DocumentController::class);

Route::get('documents/{documentId}/pdf', [DocumentController::class, 'generatePdf']);

//Route::resource('document-types', DocumentTypeController::class);
//Route::resource('document-fields', DocumentFieldController::class);
//Route::resource('documents', DocumentController::class);
//Route::resource('document-values', DocumentValueController::class);

