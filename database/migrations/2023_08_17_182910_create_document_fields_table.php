<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Define fields for each document type
 */
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_fields', function (Blueprint $table) {
            // Auto-increment primary key
            $table->id();

            // Foreign key referencing document_types
            $table->unsignedBigInteger('document_type_id');

            $table
                ->foreign('document_type_id')
                ->references('id')
                ->on('document_types')
                ->onDelete('cascade');

            // The name of the field (e.g., "Invoice Number")
            $table->string('field_name');

            // Type of the field (e.g., "string", "date", "number", "currency")
            $table->enum(
                'field_type',
                ['string', 'date', 'number', 'currency']
            );

            // Timestamps for creation and update
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_fields');
    }
};
