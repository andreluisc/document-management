<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Store each instance of a document.
 */
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            // Auto-increment primary key
            $table->id();

            // Foreign key referencing document_types
            $table->unsignedBigInteger('document_type_id');

            $table
                ->foreign('document_type_id')
                ->references('id')
                ->on('document_types')
                ->onDelete('cascade');

            // Timestamps for creation and update
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
