<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Store the values of the fields for each document.
 */
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_values', function (Blueprint $table) {
            // Auto-increment primary key
            $table->id();

            // Foreign key referencing documents
            $table->unsignedBigInteger('document_id');
            $table
                ->foreign('document_id')
                ->references('id')
                ->on('documents')
                ->onDelete('cascade');

            // Foreign key referencing document_fields
            $table->unsignedBigInteger('document_field_id');
            $table
                ->foreign('document_field_id')
                ->references('id')
                ->on('document_fields')
                ->onDelete('cascade');

            // The value of the field
            $table->string('value');

            // Timestamps for creation and update
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_values');
    }
};
