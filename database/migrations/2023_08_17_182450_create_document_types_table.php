<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Keep track of different document types
 */
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_types', function (Blueprint $table) {
            // Auto-increment primary key
            $table->id();

            // Name of the document type (e.g., "Invoice", "Contract")
            $table->string('name');

            // Path or reference to the PDF template (nullable)
            $table->string('pdf_template')->nullable();

            // Index for faster searching by name
            $table->index('name');

            // Timestamps for creation and update
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_types');
    }
};
