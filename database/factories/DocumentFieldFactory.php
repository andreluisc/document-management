<?php

namespace Database\Factories;

use App\Models\{
    DocumentField,
    DocumentType
};

use Illuminate\Database\Eloquent\{
    Factories\Factory,
    Model
};

/**
 * @extends Factory<DocumentType>
 */
class DocumentFieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<Model>
     */
    protected $model = DocumentField::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'document_type_id' => DocumentType::factory(),
            'field_name'       => $this->faker->word(),
            'field_type'       => $this->faker->randomElement(['string', 'date', 'number']),
        ];
    }
}
