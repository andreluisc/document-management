<?php

namespace Database\Factories;

use App\Models\{
    Document,
    DocumentType
};

use Illuminate\Database\Eloquent\{
    Factories\Factory,
    Model
};

/**
 * @extends Factory<Document>
 */
class DocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<Model>
     */
    protected $model = Document::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'document_type_id' => DocumentType::factory(),
        ];
    }
}
