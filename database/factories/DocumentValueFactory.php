<?php

namespace Database\Factories;

use App\Models\{
    Document,
    DocumentField,
    DocumentValue
};

use Illuminate\Database\Eloquent\{
    Factories\Factory,
    Model
};

/**
 * @extends Factory<DocumentValue>
 */
class DocumentValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<Model>
     */
    protected $model = DocumentValue::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'document_id'       => Document::factory(),
            'document_field_id' => DocumentField::factory(),
            'value'             => $this->faker->word(),
        ];
    }
}
